# W07 Guestbook Example

A simple guest book using Node, Express, BootStrap, EJS

## How to use

Open a command window in your c:\44563\w07 folder.

Run npm install -g nodemon

Run npm install to install all the dependencies in the package.json file.

Run node gbapp.js to start the server.  (Hit CTRL-C to stop.)

```
> npm install
> node gbapp.js
```

Point your browser to `http://localhost:8081`. 

On the website, you can write an entry to the Guestbook
Each entry has a Title and a content field.
